<?php
/*
Plugin Name: NRL List Sub Pages 
Plugin URI: http://robbotdev.com
Description: Adds a shortcode tag [xls] to display a list of the subpages of the current page.
Version: 1.3
Author: Robert Crawford
Author URI: http://www.robbotdev.com/
*/
function has_children($post_ID = null) {
    if ($post_ID === null) {
        global $post;
        $post_ID = $post->ID;
    }
    $query = new WP_Query(array('post_parent' => $post_ID, 'post_type' => 'any'));

    return $query;
}
function has_children_test($post_ID = null) {
    global $post;

    $children = get_pages( array( 'child_of' => $post_ID ) );
    if( count( $children ) == 0 ) {
        return false;
    } else {
        return true;
    }
}

function nrl_list_subpages($args, $post) {
    global $post;
$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order',
    'depth'          => '1',
 );


$parent = new WP_Query( $args );

if ( $args['depth'] == '1' or '2' or '3' && $parent->have_posts() ) : ?>
 <ul id="list-pages">
    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>   
        <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
            <?php if ( $args['depth'] == '2' or '3' && has_children_test($parent->ID) === true) {
                ?> <ul> <?php
                    $child = has_children($parent->ID);
                     while ( $child->have_posts() ) : $child->the_post(); ?> 
                     <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li> 
                     <?php if ( $args['depth'] == '3' && has_children_test($parent->ID) === true) {
                ?> <ul> <?php
                    $child2 = has_children($child->ID);
                     while ( $child2->have_posts() ) : $child2->the_post(); ?> 
                     <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li> 
                     <?php endwhile; ?> 
                     </ul>
         <?php   }  ?>
                     <?php endwhile; ?> 
                     </ul>
         <?php   }  ?>
    <?php endwhile; ?>
</div>
<?php endif; wp_reset_query();

}
add_shortcode('xls', 'nrl_list_subpages');